﻿using Core.Domain.Entities;
using Core.Domain.Interfaces;
using Core.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Application
{
    public class CoreService : ICoreService
    {
        private readonly CommunicatorDbContext dbContext;
        private readonly ILogger<CoreService> logger;

        public CoreService(CommunicatorDbContext dbContext, ILogger<CoreService> logger)
        {
            this.logger = logger;
            this.dbContext = dbContext;
        }

        public List<Communication> GetCommunications()
        {
            return dbContext.Communications.ToList();
        }

        public Communication GetCommunicationById(int id)
        {
            //TODO: finish method
            return dbContext.Communications.FirstOrDefault(c => c.Id == id);
        }

        public void CreateCommunication(Communication communication)
        {
            //TODO: finish method
            dbContext.Communications.Add(communication);
            dbContext.SaveChanges();
        }

        public void UpdateCommunication(Communication communication)
        {
            //TODO: finish method
            dbContext.Communications.Update(communication);
            dbContext.SaveChanges();
        }

        public void DeleteCommunication(int id)
        {
            //TODO: finish method
            var communication = GetCommunicationById(id);
            dbContext.Communications.Remove(communication);
            dbContext.SaveChanges();
        }
    }
}
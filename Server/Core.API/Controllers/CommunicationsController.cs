﻿using Core.Domain.Entities;
using Core.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Core.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommunicationsController : ControllerBase
    {
        private readonly ICoreService coreService;

        public CommunicationsController(ICoreService coreService)
        {
            this.coreService = coreService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Communication>> GetCommunications()
        {
            var communications = coreService.GetCommunications();
            return Ok(communications);
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<Communication>> GetCommunicationById(int id)
        {
            //TODO: Get record by Id
            var communication = coreService.GetCommunicationById(id);
            return Ok(communication);
        }

        [HttpPut]
        public ActionResult UpdateCommunication([FromBody] Communication communication)
        {
            //TODO: Update record
            coreService.UpdateCommunication(communication);
            return NoContent();
        }

        [HttpPost]
        public ActionResult CreateCommunication([FromBody] Communication communication)
        {
            //TODO: Create record
            coreService.CreateCommunication(communication);
            return Created($"api/communications/{communication.Id}", communication);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteCommunication(int id)
        {
            //TODO: Delete record
            coreService.DeleteCommunication(id);
            return NoContent();
        }
    }
}
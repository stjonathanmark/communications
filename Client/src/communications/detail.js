import React, { useEffect, useState } from "react";
import axios from "axios";
import { Button, TextField } from "@material-ui/core";
import { useParams } from "react-router-dom";
import { useHistory } from 'react-router-dom';

export default function CommunicationDetail() {
  const  { id } = useParams();
  const [communication, setCommunication] = useState({});
  const history = useHistory();

  const getCommunication = () => {
    axios.get(`/communications/${id}`)
    .then(function (response) {
      // handle success
      if (!isNaN(id)) {
        setCommunication(response.data);
        console.log(communication);
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
  }

   useEffect(() => {
    // TODO: fetch communication detail data
    getCommunication();
  }, []);

  const sendCommunication = data => {
    axios({
      method: id === "create" ? 'post' : 'put',
      url: '/communications',
      data
    })
    .then(() => {
        history.push("/");
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    });
  }

  const deleteCommunication = () => {
    axios({
        method: 'delete',
        url: `/communications/${id}`,
      })
      .then(() => {
          history.push("/");
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  }

  const handleChange = (e) => {
    const { id, value } = e.target;
    setCommunication({ ...communication, [id]: value });
  }


    // TODO: Show From, To, Subject, and Message fields. Allow for Create, Update, and Delete
  return (
    <>
      <div className="form-control">
        <TextField
            className="text-input"
            id="from"
            label="From"
            variant="outlined"
            value={communication.from}
            onChange={handleChange}
        />
      </div>
      <div className="form-control">
      <TextField
        id="to"
        label="To"
        variant="outlined"
        value={communication.to}
        onChange={handleChange}
      />
      </div>
      <div className="form-control">
      <TextField
        id="subject"
        label="Subject"
        variant="outlined"
        value={communication.subject}
        onChange={handleChange}
      />
      </div>
      <div className="form-control">
      <TextField
        id="message"
        label="Message"
        variant="outlined"
        value={communication.message}
        onChange={handleChange}
      />
      </div>
      
      <div className="buttons">
        { 
            id !== "create" 
                ?
                <> 
                    <Button variant="contained" color="secondary" onClick={deleteCommunication}>
                        Delete
                    </Button> 
                    &nbsp;&nbsp;
                </>
                : null 
        }
        
        <Button variant="contained" color="primary" onClick={() => { sendCommunication(communication) }}>
            Send
        </Button>
        
        &nbsp;&nbsp;

        <Button variant="contained" onClick={() => { history.push("/") }}>
            Cancel
        </Button>
      </div>
  </>
  );
}

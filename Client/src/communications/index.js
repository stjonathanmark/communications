import React, { useState, useEffect } from "react";
import axios from "axios";
import { Table, TableRow, TableCell, TableBody, TableHead, Button, TableContainer  } from "@material-ui/core";
import { useHistory } from 'react-router-dom';

export default function Communications() {
const [loading, setLoading] = useState(true);
const [communications, setCommunications] = useState();
const history = useHistory();

const getCommunications = () => {
  setLoading(true);

  axios.get("/Communications")
  .then(function (response) {
    console.log(response);
    setCommunications(response.data);
    setLoading(false);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
}

  useEffect(() => {
    getCommunications();
  }, []);

  if(loading){
    return <div>Loading...</div>
  }

  const goToDetails = (id) => {
    history.push(`/${id}`);
  }

  //TODO: Make individual rows clickable so you can drill down into the details
  return (
    <>
    <Button variant="contained" color="primary" onClick={() => { history.push('/create') }}>
      Create New
    </Button>
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>From</TableCell>
            <TableCell>To</TableCell>
            <TableCell>Subject</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {communications.map((row) => (
            <TableRow key={row.id} className="communication-row" onClick={() => { goToDetails(row.id) }}>
              <TableCell>{row.from}</TableCell>
              <TableCell>{row.to}</TableCell>
              <TableCell>{row.subject}</TableCell>
            </TableRow>
          ))}
        </TableBody>
        </Table>
      </TableContainer>
      </>
  );
}